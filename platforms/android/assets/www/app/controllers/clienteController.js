appControllers.controller('ClienteCtrl', ['$scope', '$location', 'Cliente', 'setCreds', function($scope, $location, Cliente, setCreds) {
    $scope.submit = function() {
        var postData = {
            "nome": $scope.nome,
            "telefone": $scope.telefone,
            "cpf": $scope.cpf,
            "email": $scope.email,
            "usuario": $scope.usuario,
            "senha": $scope.senha,
            "endereco": {
                "logradouro": $scope.logradouro,
                "numero": $scope.numero,
                "bairro": $scope.bairro,
                "cidade": $scope.cidade,
                "cep": $scope.cep
            }
        };
        Cliente.salvar.salvar({}, postData,
            function success(response) {
                console.log("Success:" + JSON.stringify(response));
                setCreds($scope.usuario, $scope.senha, response.id);
                $scope.activetab = $location.path('/');
            },
            function error(errorResponse) {
                console.log("Error:" + JSON.stringify(errorResponse));
            }
        );
    }    
}]);
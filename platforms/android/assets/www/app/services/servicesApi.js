var appServicesApi = angular.module('appServicesApi', ['ngResource']);

appServicesApi.factory('Cliente', ['$resource',
    function ($resource) {
        return {
            salvar: $resource('http://agendae.mfs.eng.br/api/cliente', {}, {
                salvar: { method: 'POST', cache: false, isArray: false },
            }),
            getCliente: $resource('http://agendae.mfs.eng.br/api/cliente/id/:idcliente', {}, {
                go: { method: 'GET', cache: false, isArray: false },
                edit: { method: 'POST', cache: false, isArray: false }
            })
        }
    }
]);

appServicesApi.factory('Login', ['$resource',
    function ($resource) {
        return $resource('http://agendae.mfs.eng.br/api/cliente/login', {}, {
            login: { method: 'POST', cache: false, isArray: false }
        });
    }
]);

appServicesApi.factory('Profissao', ['$resource',
    function ($resource) {
        Profissao = $resource('http://agendae.mfs.eng.br/api/profissao', {}, {
            listar: { method: 'GET', cache: false, isArray: true }
        })

        return Profissao;
    }
]);

appServicesApi.factory('Especialista', ['$resource', function ($resource) {
    especialista = $resource('http://agendae.mfs.eng.br/api/especialista/:profissoesid', {}, {
        listar: { method: 'GET', cache: false, isArray: true }
    })

    return especialista;
}]);
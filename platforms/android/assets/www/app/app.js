var app = angular.module('app', [
    'ngRoute',
    'appControllers',
    'appServicesBusiness',
    'appServicesApi',
    'appDirectives',
    'mgo-angular-wizard',
    'ui.select',
    'ngSanitize'
]);

app.config(function ($routeProvider, $locationProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'app/views/home.html',
            controller: 'HomeCtrl',
        })

        .when('/solicitacao', {
            templateUrl: 'app/views/solicitacao.html',
            controller: 'SolicitacaoCtrl',
        })

        .when('/solicitacao/:pagina', {
            templateUrl: function (param) {
                return 'app/views/solicitacao/' + param.pagina + '.html'
            },
            controller: 'SolicitacaoCtrl'
        })

        .when('/historico', {
            templateUrl: 'app/views/historico.html',
            controller: 'HistoricoCtrl',
        })

        .when('/perfil', {
            templateUrl: 'app/views/perfil.html',
            controller: 'PerfilCtrl',
        })

        .when('/perfilreload', {
            templateUrl: 'app/views/perfil.html',
            controller: 'Perfil2Ctrl',
        })
        
        .when('/login', {
            templateUrl: 'app/views/login.html',
            controller: 'LoginCtrl'
        })
        .when('/logout', {
            templateUrl: 'app/views/login.html',
            controller: 'LogoutCtrl'
        })
        .when('/cadastro', {
            templateUrl: 'app/views/cadastro.html',
            controller: 'ClienteCtrl'
        })
        // caso não seja nenhum desses, redirecione para a rota '/'
        .otherwise({ redirectTo: '/' });

    $locationProvider.html5Mode(false).hashPrefix('!');
});
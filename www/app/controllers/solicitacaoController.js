appControllers.controller('SolicitacaoCtrl', ['$rootScope', '$scope', '$location', '$http', 'Profissao', 'Especialista', 'checkCreds', 'getToken', function($rootScope, $scope, $location, $http, Profissao, Especialista, checkCreds, getToken) {
    if (!checkCreds()) {
        $scope.activetab = $location.path('/login');
    }
    $http.defaults.headers.common['Authorization'] = 'Basic ' + getToken();

    Profissao.listar({},
        function success(response) {
            console.log(JSON.stringify(response));
            $scope.profissao = response;
            $scope.selected = $scope.profissao[0]
        },
        function error(errorResponse) {
            console.log("Error:" + JSON.stringify(errorResponse));
        }
    );

    $scope.goBuscarEspecialista = function() {
        console.log($scope);
        Especialista.listar({ profissoesid: $scope.selected._id },
            function success(response) {
                console.log(JSON.stringify(response));
                $rootScope.especialistas = response;
                $scope.activetab = $location.path("/solicitacao/listar_esp");
            },
            function error(errorResponse) {
                console.log("Error:" + JSON.stringify(errorResponse));
            });

        /* $scope.activetab = $location.path('/solicitacao/listar_especialista');*/
    };

    $scope.verAgendaEspecialista = function(idespecialista) {
        $rootScope.idespecialista = idespecialista;
        //console.log("idEspecialista"+ idespecialista);
    };

    $scope.cancel = function() {
        $scope.activetab = $location.path('/');
    }
}]);
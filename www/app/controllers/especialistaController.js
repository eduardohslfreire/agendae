appControllers.controller('EspecialistaCtrl', ['$scope', '$location', 'checkCreds', function($scope, $location, checkCreds) {
    if (!checkCreds()) {
        $scope.activetab = $location.path('/login');
    }
}]);
var appControllers = angular.module('appControllers', []);

appControllers.controller('HomeCtrl', ['$scope', '$location', 'checkCreds', function ($scope, $location, checkCreds) {
    if (!checkCreds()) {
        $scope.activetab = $location.path('/login');
    }
}]);

appControllers.controller('HistoricoCtrl', ['$scope', '$location', 'checkCreds', function ($scope, $location, checkCreds) {
    if (!checkCreds()) {
        $scope.activetab = $location.path('/login');
    }
}]);

appControllers.controller('PerfilCtrl', ['$scope', '$location', 'checkCreds', 'getId', 'Cliente', function ($scope, $location, checkCreds, getId, Cliente) {
    if (!checkCreds()) {
        $scope.activetab = $location.path('/login');
    }
    var id = getId();
    $scope.status = "editar";

    Cliente.getCliente.go({ idcliente: id },
        function success(response) {
            // console.log("Success:" + JSON.stringify(response));            
            $scope.nome = response.nome;
            $scope.telefone = response.telefone;
            $scope.cpf = response.cpf;
            $scope.email = response.email;
            $scope.usuario = response.usuario;
            $scope.senha = response.senha;
            $scope.logradouro = response.endereco.logradouro;
            $scope.numero = response.endereco.numero;
            $scope.bairro = response.endereco.bairro;
            $scope.cidade = response.endereco.cidade;
            $scope.cep = response.endereco.cep;
        },
        function error(errorResponse) {
            console.log("Error:" + JSON.stringify(errorResponse));
        }
    );

    $scope.cancelar = function () {
        console.log("cancelar");
        $scope.activetab = $location.path('/home');
    }

    $scope.sair = function () {
        console.log("sair");
        $scope.activetab = $location.path('/logout');
    }

    $scope.editSave = function () {
        if ($scope.status === "editar") {
            //console.log("Modo Edicao");
            $scope.status = "salvar";
        } else {//salvar
            var postData = {
                "nome": $scope.nome,
                "telefone": $scope.telefone,
                "cpf": $scope.cpf,
                "email": $scope.email,
                "usuario": $scope.usuario,
                "senha": $scope.senha,
                "endereco": {
                    "logradouro": $scope.logradouro,
                    "numero": $scope.numero,
                    "bairro": $scope.bairro,
                    "cidade": $scope.cidade,
                    "cep": $scope.cep
                }
            };
            Cliente.getCliente.edit({ idcliente: id },postData,
                function success(response) {
                },
                function error(errorResponse) {
                    console.log("Error:" + JSON.stringify(errorResponse));
                }
            );
            $scope.status = "editar";
        }
    }

}]);

appControllers.controller('Perfil2Ctrl', ['$scope', '$location', 'checkCreds', function ($scope, $location, checkCreds) {
    if (!checkCreds()) {
        $scope.activetab = $location.path('/login');
    }
    $scope.activetab = $location.path('/perfil');
}]);

appControllers.controller('LoginCtrl', ['$scope', '$location', 'Login', 'setCreds', 'checkCreds', '$http',
    function LoginCtrl($scope, $location, Login, setCreds, checkCreds, $http) {
        if (checkCreds()) {
            $scope.activetab = $location.path('/');
        }


        $scope.submit = function () {
            $scope.sub = true;
            var postData = {
                "username": $scope.username,
                "password": $scope.password
            };
            //$http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
            /*  setCreds($scope.username, $scope.password);
              $scope.activetab = $location.path('/');*/
            //$httpProvider.defaults.headers = {'Content-Type': 'application/json;charset=utf-8'};


            Login.login({}, postData,
                function success(response) {
                    console.log("Success:" + JSON.stringify(response));
                    $scope.error = "";
                    if (response.authenticated) {
                        setCreds($scope.username, $scope.password, response.id);
                        $scope.activetab = $location.path('/');
                    } else {
                        //$scope.error = "Login Failed"
                        console.log("Login Failed");
                        $scope.error = "usuário/senha inválidos!"
                    }
                },
                function error(errorResponse) {
                    console.log("Error:" + JSON.stringify(errorResponse));
                }
            );
        };
    }
]);

appControllers.controller('LogoutCtrl', ['$scope', '$location', 'deleteCreds',
    function LogoutCtrl($scope, $location, deleteCreds) {
        deleteCreds();
        $scope.activetab = $location.path('/login');
    }
]);